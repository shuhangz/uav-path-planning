clear
clc
close all
addpath('function');

exteriorsPath = 'data\Exteriors.mat'; % 外方位元素：x,y,z,omega,phi,kappa。第一行为起飞点。规划航线时忽略omega,phi,kappa
meshPath = 'data\Tianqin_Mesh_noNormal.ply';
UAVParams = struct;
    UAVParams.maxSpeed = 5; % 飞行速度
    UAVParams.acceleration = 2.5; % 加速度
    UAVParams.minAltitude = 15; % 航点最低飞行高度
    UAVParams.minDistWP = 1;

%% 读取数据
load('data\Exteriors.mat');
% 读取mesh文件，用于冲突检测
fv = read_ply2(meshPath);
v = fv.vertices';
f = fv.faces';
meshModel = opcodemesh(v,f);  

takeOffPosition = EO_sortedwithTakeoff(1,1:3); % 起飞点坐标
waypointsForImaging = EO_sortedwithTakeoff(2:end, 1:3); % 拍照点坐标

%% 规划路径
% 在此写路径规划的代码（函数），输出waypointsPlanned、sorting
% 如用其他语言编写路径规划的代码，在此写读取waypointsPlanned、sorting结果的代码

waypointsPlanned = waypointsForImaging; % 规划的无人机路径点，可以加入新的路径点，但必须途径所有拍照点
% sorting = randperm(size(waypointsPlanned,1));% 这个变量即为访问waypointsPlanned的顺序，如 5,3,2,1,4（不包含起飞点）
sorting = 1:size(waypointsPlanned,1); % 这个航线是按顺序执行，但有问题
%% 画图
figure('Name','需要经过的拍照点');
ply_display(meshPath);
hold on;
plotCameras(EO_sortedwithTakeoff(2:end,:),[0 0 0], 'red', 2);

figure('Name','规划航线');
ply_display(meshPath);
hold on;
plotCameras(EO_sortedwithTakeoff(2:end,:),[0 0 0], 'red', 2);
hasCollision = plotFlightPath(waypointsPlanned, sorting, takeOffPosition, meshModel);

%% 结果判定
% 有效航线的判定标准
% 1. 航线与mesh没有冲突
% 2. 航线途径所有的拍照点waypointsForImaging
% 3. 航线所有航点不低于最低飞行高度
% 4. 当前航点与下一航点间距1 m以上
% 
% 航线规划效果判定标准：执行时间越短越好
[isValid,flightTime,fLength]=evaluateFlightPath(waypointsPlanned, sorting, takeOffPosition,waypointsForImaging, UAVParams);
isFlightPathValid = (~hasCollision) && isValid;

fprintf("该航线总共有%d个航点，航线长度为%f m，执行时间%f s。\n",size(waypointsPlanned,1),fLength,flightTime);
if isFlightPathValid
    disp("航线检查通过！");
else
    disp("航线有问题，请重新规划。");
end



