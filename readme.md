# 编程竞赛：无人机三维航线规划
## 任务描述
给定一个三维场景，`n`台无人机所需经过的拍照点以及一个无人机起飞点，规划由`N>=n`个航点组成的单无人机航线，要求如下：
1. 航线与场景建筑没有冲突，即飞机不会撞上建筑；
2. 航线途径所有的拍照点；
3. 航线所有航点不低于最低飞行高度；
4. 当前航点与下一航点间距1 m 以上。

规划结果存储于两个变量中：`waypointsPlanned`, `sorting`。

- `waypointsPlanned`为`N*3`数组，每一行为一个航点的`x,y,z`坐标（不包含起飞点）。
- `sorting`为`N*1`数组，存储着访问`waypointsPlanned`的顺序，如：5,3,2,1,4（不包含起飞点）

起飞点坐标已给出：`935.350000000000	376.930000000000	18.4720553687003`

![场景与拍照点](./img/shutterpoint.png "场景与拍照点")

图：场景与拍照点

![示例规划](./img/planned.png)

图：示例规划航线（这个航线有问题，不要照搬）

### 评判标准
无人机从起飞点起飞，按照规划顺序飞行至各航点，最后返回起飞点（规划的航线不必包含起飞点，程序自动加入起飞点）。规划的航线必需符合以上要求，在符合以上要求的前提下，执行时间越少越好。

无人机在每个航点悬停，航线执行时加/减速度`2.5 m/s^2`，飞行速度`5 m/s`。执行时间由`\function\evaluateFlightPath.m`函数计算。`sample.m`代码中会自动评判航线是否符合以上要求，自动计算执行时间。


### 给定文件描述
`sample.m` 示例运行文件。读取场景及无人机拍照点，并依据评判标准评价规划航线的质量。本代码在MATLAB R2021a，Windows 10 x64环境下测试通过。

#### **\data**
场景文件：`\Tianqin_Mesh_noNormal.ply`
无人机拍照点文件：`Exteriors.mat`

#### **\function**
该文件夹里存储了`sample.m`运行所需的函数。关键函数有：
1. `checkFlightPathCollisionWithModel`，判断航线线段是否与建筑mesh有交点；
2. `evaluateFlightPath`评价航线可用性。
3. `plotFlightPath`评价航线是否有冲突，并画出航线。

## 相关知识
无人机航线规划的问题不论是二维，还是三维，均可抽象建模成旅行商问题(travelling salesman problem)。[相关知识](https://ww2.mathworks.cn/help/optim/ug/travelling-salesman-problem.html)

航线规划时，可以规划额外的航点，让无人机**绕过障碍物**。例如可以使用A-star、RRT等算法：
1. https://ww2.mathworks.cn/matlabcentral/fileexchange/26248-a-a-star-search-for-path-planning-tutorial 
2. https://ww2.mathworks.cn/matlabcentral/fileexchange/56877-astar-algorithm 
3. https://ww2.mathworks.cn/en/discovery/path-planning.html 

![obs](./img/Path-planning-and-obstacle-avoidance-of-the-proposed-model_W640.jpg)

图：绕过障碍物


## 结果提交
将可运行的代码、采用的库文件、第三方代码等，以及对应的运行说明文件打包发送至张书航邮箱`zhangsh52@mail.sysu.edu.cn`，收到邮件的时间必需早于截止时间。运行说明包含：
1. 应该运行哪个.m文件；
2. 若程序非matlab代码，需说明如何配置运行环境。
3. 如何调用`evaluateFlightPath`以及`plotFlightPath`评价规划好的航线。
   
如果画图和评价代码有问题，请在此提交Pull request，或者开启issue。