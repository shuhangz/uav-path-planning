function hasCollision = plotFlightPath(waypoints,sorting,depot,meshModel)
%PLOTFLIGHTPATH 画出三维航线图
%   输入：waypoints, n*3数组，内有航点的三维坐标；sorting：n*1数组，标志访问航点的顺序
hasCollision = false;
sortedWaypoints = waypoints(sorting,:);
sortedWaypoints = [depot;sortedWaypoints;depot];
% vectorForQuiver=zeros(size(sortedWaypoints,1)-1,3);
hold on;
% 画航线
plot3(sortedWaypoints(:,1),sortedWaypoints(:,2),sortedWaypoints(:,3), '.-','Color','green','LineWidth',1);
for i=1:size(sortedWaypoints,1)-1
    point = sortedWaypoints(i,1:3);
    pointNext = sortedWaypoints(i+1,1:3);
    vec=(pointNext-point);
%     vectorForQuiver(i,:) = vec/norm(vec)*20;
    
    if (i>1 && i<size(sortedWaypoints,1)-1)
        text(point(1),point(2),point(3),sprintf('WP %d',i-1), 'Color','green');
    end
    [isCollied, dist,pt]=checkFlightPathCollisionWithModel(point,pointNext,meshModel);
    if isCollied
        hasCollision=true;
        plot3(pt(1),pt(2),pt(3),'*','Color','red','MarkerSize',20);
        fprintf('Collision occurs between waypoint %d and %d, collision distance is %f. \n',i,i+1,dist);
        tmp = [point;pointNext];
        line(tmp(:,1),tmp(:,2),tmp(:,3), 'Color', 'red', 'LineWidth',3);
    end
end


if ~isempty(depot)
    plot3(depot(1),depot(2),depot(3),'.','Color','green','MarkerSize',20);
    text(depot(1),depot(2),depot(3),'Take off position', 'Color','red');
end
% quiver3(sortedWaypoints(1:end-1,1),sortedWaypoints(1:end-1,2),sortedWaypoints(1:end-1,3),vectorForQuiver(:,1),vectorForQuiver(:,2),vectorForQuiver(:,3),0,'Color','green','LineWidth',1);
axis equal
view(70,45)



