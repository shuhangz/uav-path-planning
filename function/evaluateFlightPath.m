function [isValid, flightTime, length] = evaluateFlightPath(waypoints,sorting,depot,waypointsForImaging, UAVParams)
%EVALUATEFLIGHTPATH 此处显示有关此函数的摘要
%   此处显示详细说明
isValid=true;
flightTime = 0;
length = 0;
accDccTime = UAVParams.maxSpeed/UAVParams.acceleration; % 无人机加速或减速的时间
accDccDist = UAVParams.acceleration*accDccTime*accDccTime; % 无人机加、减速运动至5 m/s的距离

sortedWaypoints = waypoints(sorting,:);
sortedWaypoints = [depot;sortedWaypoints;depot];
isImagingWaypoint = zeros(size(sortedWaypoints,1),1); % 判断各航点是否拍照点


isWaypointForImagingPassed = zeros(size(waypointsForImaging,1),1);
% 有效航线的判定标准
% 1. 航线与mesh没有冲突
% 2. 航线途径所有的拍照点waypointsForImaging
% 3. 航线所有航点不低于最低飞行高度
% 4. 当前航点与下一航点间距1 m以上

% 计算航线长度、判断航点间距
for i=1:size(sortedWaypoints,1)-1
    point = sortedWaypoints(i,1:3);
    pointNext = sortedWaypoints(i+1,1:3);
    vec=(pointNext-point);
    dist = norm(vec);
    length = length + dist;    
    if dist<UAVParams.minDistWP
        fprintf("Distance between waypoint %d and %d is too short!\n",i-1,i);
        isValid=false;
    end
    
end

for i=1:size(sortedWaypoints,1)
    
    point = sortedWaypoints(i,1:3);
    for j=1:size(waypointsForImaging,1)
        point2=waypointsForImaging(j,1:3);
        vec2=point2-point;
        if norm(vec2) < 1e-4
            isWaypointForImagingPassed(j)=1;
            isImagingWaypoint(i)=1;
        end
    end
    
    if point(3)<UAVParams.minAltitude
        fprintf("Waypoint %d is below safety altitude!\n",i-1);
        isValid=false;
    end
    
end

for k=1:size(isWaypointForImagingPassed)
    if ~isWaypointForImagingPassed(k)
        fprintf("The flight path does not cover shutter point %d.\n",k);
        isValid=false;
    end
end


% calculate overall time
distSegment = 0;
for i=1:size(sortedWaypoints,1)-1
    point = sortedWaypoints(i,1:3);
    pointNext = sortedWaypoints(i+1,1:3);
    vec=(pointNext-point);
    dist = norm(vec);
    distSegment = distSegment + dist;
    isImagingWayPointNext = isImagingWaypoint(i+1);
    if ((~isImagingWayPointNext) && (i<size(sortedWaypoints,1)-1))
        continue; % 如果下一个航点不是拍照点，无人机不减速。
    else
        if distSegment > accDccDist % 无人机匀加速至5 m/s，然后到拍照点减速至0
            flightTime = flightTime + (distSegment-accDccDist)/UAVParams.maxSpeed + accDccTime*2;
        else % 如果航点间距离不够无人机加速至5 m/s
            flightTime = flightTime + sqrt(distSegment/UAVParams.acceleration);
        end
        distSegment=0;
    end
    
    
end



end

