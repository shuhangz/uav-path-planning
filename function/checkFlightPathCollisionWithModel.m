function [hasCollision,distCollide,pointCollide] = checkFlightPathCollisionWithModel(point1,point2,meshModel)
%CHECKFLIGHTPATHCOLLISIONWITHMODEL 此处显示有关此函数的摘要
%   此处显示详细说明
vec = point2-point1;
[hasCollision,distCollide,~,~,pointCollide] = meshModel.intersect(point1',vec');
if (hasCollision && (distCollide > norm(vec)) )
    hasCollision=false;
    distCollide=0;
    return;
end
end

