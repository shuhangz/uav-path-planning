function p=plotCameras(EO, globalShift, colorString, camSize)
showNumbers = false;
nCamera=size(EO,1);

if nargin < 4
    camSize=1;
end

if (~isnumeric(colorString) && colorString == "Grey")
    colorString = [.7 .7 .7];
end
    for i=1:nCamera
        EO_c=EO(i,:);
        loc=EO_c(1:3)+globalShift;    

        Rw=m_rotx(EO_c(4));
        Rp=m_roty(EO_c(5));
        Rk=m_rotz(EO_c(6));

        R=Rw*Rp*Rk*m_rotx(180);
        
%         R=Rk*Rp*Rw*rotx(180);
%         R=R';
%         eulZYX = rad2deg(rotm2eul(R));

        cam=plotCamera('Location',loc,'Orientation',R,'Opacity',0,'Color',colorString,'Size',camSize);
        if (~isequal(colorString, [.7 .7 .7]) && showNumbers)
           t=text(loc(:,1), loc(:,2) ,loc(:,3),num2str(i)); % camera name
           t.Color='blue';
           t.FontSize=15;
        end

    %      cameras=findall(gcf,'Label','Cam');
    end
end

function rotmat=m_rotx(alpha)
   rotmat = [1 0 0;0 cosd(alpha) -sind(alpha); 0 sind(alpha) cosd(alpha)];   
end

function rotmat=m_roty(beta)
   rotmat = [cosd(beta) 0 sind(beta); 0 1 0; -sind(beta) 0 cosd(beta)];   
end

function rotmat=m_rotz(gamma)
   rotmat = [cosd(gamma) -sind(gamma) 0; sind(gamma) cosd(gamma) 0; 0 0 1];   
end

